var tiger = {
    showRedFlagByStatus : function(view) {
        var graphdef = {
            categories : ['Not Started', 'On Hold'],
            dataset : {
                'Not Started' : [
                    { name : 'Property', value : 2 },
                    { name : 'Gaming', value : 0 },
                    { name : 'BOH', value : 3 },
                    { name : 'Hotel & FOH', value : 2 }
                ],

                'On Hold' : [
                    { name : 'Property', value : 0 },
                    { name : 'Gaming', value : 2 },
                    { name : 'BOH', value : 0 },
                    { name : 'Hotel & FOH', value : 2 }
                ]
            }
        };

        var chart = uv.chart ('StackedBar', graphdef, {
            meta : {
                caption : 'PROJECTS IN RED FLAG STATUS',
                subcaption : 'total: 12',
                hlabel : 'NO. OF PROJECTS',
                //vlabel : 'BUSINESS UNIT',
                position: '#rfstatus'
            },
            graph: {
                responsive: true,
                custompalette: ['#600', '#f00']
            },
            bar: {
                textcolor: '#ccc',
                fontsize: 24,
                fontweight: 'normal'
            },
            caption: {
                strokecolor: '#ccc',
                fontsize: 30
            },
            subCaption: {
                strokecolor: '#ccc',
                fontsize: 24
            },
            label: {
                fontsize: 24
            },
            legend: {
                fontsize: 12
            },
            axis: {
                fontsize: 20
            },
            tooltip: {
                format: '%l'
            }
        });

        $('.uv-chart > g > g').on('tap, click', function(){

            var itm = $(this).context;
            var title = $(itm).find('title').html();
            var space = title.indexOf(' ');
            var url = title + '.html';
            if (space > 0) {
                var url = title.substr(0, title.indexOf(' ')) + '.html';
            }

            view.router.loadPage(url);

            //var url = 'redflags.html';
            //
            //if ($(itm).attr('class') == 'cge-not-started') {
            //    url = url + '#notstarted';
            //} else if ($(itm).attr('class') == 'cge-on-hold') {
            //    url = url + '#onhold';
            //}

            $('#allvariance .cl-behind-schedule').attr("transform","translate(255,0)");
            $('#allvariance .cl-ahead-of-schedule').attr("transform","translate(110,0)");
            $('#allstatus .cl-being-evaluated').attr("transform","translate(250,0)");
            $('#allstatus .cl-selected').attr("transform","translate(375,0)");
            $('#allstatus .cl-on-hold').attr("transform","translate(145,20)");

        });
    },
    showRedFlagByVariance : function(view) {
        var graphdef = {
            categories : ['Behind Schedule'],
            dataset : {
                'Behind Schedule' : [
                    { name : 'Property', value : 0 },
                    { name : 'Gaming', value : 3 },
                    { name : 'BOH', value : 0 },
                    { name : 'Hotel & FOH', value : 0 }
                ]
            }
        };

        var chart = uv.chart ('StackedBar', graphdef, {
            meta : {
                caption : 'PROJECTS IN RED FLAG VARIANCE',
                subcaption : 'total: 12',
                hlabel : 'NO. OF PROJECTS',
                //vlabel : 'BUSINESS UNIT',
                position: '#rfvariance'
            },
            graph: {
                responsive: true,
                custompalette: ['#f00']
            },
            bar: {
                textcolor: '#ccc',
                fontsize: 24,
                fontweight: 'normal'
            },
            caption: {
                strokecolor: '#ccc',
                fontsize: 30
            },
            subcaption: {
                strokecolor: '#ccc',
                fontsize: 24
            },
            label: {
                fontsize: 24
            },
            legend: {
                fontsize: 12
            },
            axis: {
                fontsize: 20
            },
            tooltip: {
                format: '%l'
            }
        });

        $('.uv-chart > g > g').on('tap, click', function(){
            var itm = $(this).context;
            //var title = $(itm).find('title').html();
            //var space = title.indexOf(' ');
            //var url = title + '.html';
            //if (space > 0) {
            //    var url = title.substr(0, title.indexOf(' ')) + '.html';
            //}
            //
            //view.router.loadPage(url);


            var url = 'redflags.html';
            var itm = $(this).context;
            if ($(itm).attr('class') == 'cge-behind-schedule') {
                url = url + '#behind';
            }

            view.router.loadPage(url);
        });
    },

    showAllByStatus : function(view) {
        var graphdef = {
            categories : ['Not Started', 'Preliminary Sourcing', 'Being Evaluated', 'Selected', 'Being Implemented', 'On Hold'],
            dataset : {
                'Not Started' : [
                    { name : 'Property', value : 2 },
                    { name : 'Gaming', value : 0 },
                    { name : 'BOH', value : 3 },
                    { name : 'Hotel & FOH', value : 2 }
                ],
                'Preliminary Sourcing' : [
                    { name : 'Property', value : 0 },
                    { name : 'Gaming', value : 1 },
                    { name : 'BOH', value : 0 },
                    { name : 'Hotel & FOH', value : 7 }
                ],

                'Being Evaluated' : [
                    { name : 'Property', value : 2 },
                    { name : 'Gaming', value : 3 },
                    { name : 'BOH', value : 2 },
                    { name : 'Hotel & FOH', value : 6 }
                ],

                'Selected' : [
                    { name : 'Property', value : 1 },
                    { name : 'Gaming', value : 4 },
                    { name : 'BOH', value : 5 },
                    { name : 'Hotel & FOH', value : 5 }
                ],

                'Being Implemented' : [
                    { name : 'Property', value : 2 },
                    { name : 'Gaming', value : 6 },
                    { name : 'BOH', value : 4 },
                    { name : 'Hotel & FOH', value : 1 }
                ],

                'On Hold' : [
                    { name : 'Property', value : 0 },
                    { name : 'Gaming', value : 3 },
                    { name : 'BOH', value : 0 },
                    { name : 'Hotel & FOH', value : 2 }
                ]
            }
        };

        var chart = uv.chart ('StackedBar', graphdef, {
            meta : {
                caption : 'ALL PROJECTS by STATUS',
                subcaption : 'total: 61',
                hlabel : 'NO. OF PROJECTS',
                //vlabel : 'BUSINESS UNIT',
                position: '#allstatus'
            },
            graph: {
                responsive: true,
                custompalette: ['#600', 'yellow', 'lightblue', 'green', 'blue', '#f00']
            },
            bar: {
                textcolor: '#ccc',
                fontsize: 24,
                fontweight: 'normal'
            },
            caption: {
                strokecolor: '#ccc',
                fontsize: 30
            },
            subCaption: {
                strokecolor: '#ccc',
                fontsize: 24
            },
            label: {
                fontsize: 24
            },
            legend: {
                fontsize: 18
            },
            axis: {
                fontsize: 20
            }
        });

        $('.uv-chart > g > g').on('tap, click', function(){
            view.router.loadPage('redflags.html');
        });

        $('#allvariance .cl-behind-schedule').attr("transform","translate(255,0)");
        $('#allvariance .cl-ahead-of-schedule').attr("transform","translate(110,0)");
        $('#allstatus .cl-being-evaluated').attr("transform","translate(250,0)");
        $('#allstatus .cl-selected').attr("transform","translate(375,0)");
        $('#allstatus .cl-on-hold').attr("transform","translate(145,20)");
    },

    showAllByVariance : function(view) {
        var graphdef = {
            categories : ['On Schedule', 'Ahead of Schedule', 'Behind Schedule'],
            dataset : {
                'On Schedule' : [
                    { name : 'Property', value : 4 },
                    { name : 'Gaming', value : 14 },
                    { name : 'BOH', value : 10 },
                    { name : 'Hotel & FOH', value : 6 }
                ],

                'Ahead of Schedule' : [
                    { name : 'Property', value : 3 },
                    { name : 'Gaming', value : 1 },
                    { name : 'BOH', value : 4 },
                    { name : 'Hotel & FOH', value : 17 }
                ],

                'Behind Schedule' : [
                    { name : 'Property', value : 0 },
                    { name : 'Gaming', value : 2 },
                    { name : 'BOH', value : 0 },
                    { name : 'Hotel & FOH', value : 0 }
                ]
            }
        };

        var chart = uv.chart ('StackedBar', graphdef, {
            meta : {
                caption : 'ALL PROJECTS by VARIANCE',
                subcaption : 'total: 61',
                hlabel : 'NO. OF PROJECTS',
                //vlabel : 'BUSINESS UNIT',
                position: '#allvariance'
            },
            graph: {
                responsive: true,
                custompalette: ['green', 'blue', 'red']
            },
            bar: {
                textcolor: '#ccc',
                fontsize: 24,
                fontweight: 'normal'
            },
            caption: {
                strokecolor: '#ccc',
                fontsize: 30
            },
            subcaption: {
                strokecolor: '#ccc',
                fontsize: 24
            },
            label: {
                fontsize: 24
            },
            legend: {
                fontsize: 12
            },
            axis: {
                fontsize: 20
            }
        });

        $('.uv-chart > g > g').on('tap, click', function(){
            view.router.loadPage('redflags.html');
        });

        $('#allvariance .cl-behind-schedule').attr("transform","translate(255,0)");
        $('#allvariance .cl-ahead-of-schedule').attr("transform","translate(110,0)");
        $('#allstatus .cl-being-evaluated').attr("transform","translate(250,0)");
        $('#allstatus .cl-selected').attr("transform","translate(375,0)");
        $('#allstatus .cl-on-hold').attr("transform","translate(145,20)");
    },

    showAllProjects : function(view) {
        var graphdef = {
            categories : ['BU'],
            dataset : {
                'BU' : [
                    { name : 'Property', value : 11 },
                    { name : 'Hotel & FOH', value : 38 },
                    { name : 'BOH', value : 23 },
                    { name : 'Gaming', value : 28 }
                ]
            }
        };

        var chart = uv.chart ('Pie', graphdef, {
            meta : {
                caption : 'ALL PROJECTS',
                subcaption: 'BY BUSINESS UNIT',
                position: '#allprojects'
            },
            margin: {
                top: 60,
                right: 150,
                bottom: 10,
                left: 10
            },
            dimension: {
                width: 300,
                height: 180
            },
            graph: {
                responsive: true,
                custompalette: ['#FFDEAD', '#B8860B', '#DCB122', '#EEC86D']
            },
            pie: {
                textcolor: '#ccc',
                fontsize: 18,
                fontweight: 'normal'
            },
            caption: {
                fontsize: 18
            },
            subCaption: {
                fontsize: 16
            },
            legend: {
                fontsize: 16,
                position: 'right'
            }
        });

        $('.uv-chart').on('tap, click', function(){
            view.router.loadPage('charts.html');
        });

        $.each($('.uv-chart > g text'), function(index, obj){$(obj).html( $(obj).html() + '%');} );
    },

    getMessages: function(url, target, count) {
        $$.get(
            url + '/api/tiger/message',
            {},
            function(data) {
                var json = JSON.parse(data);
                if (count == -1) {
                    count = json.length;
                }
                $$(target).html('');
                for(i=0; i < count; i++) {
                    var sent = new Date(json[i].created_at);
                    var month = sent.getMonth() + 1;
                    var day = sent.getDate();
                    var displaydate = month + '/' + day;
                    var ili = '<li class="accordion-item">' +
                        '<a href="#" class="item-link item-content">' +
                            '<div class="item-inner">' +
                                '<div class="item-title-row">' +
                                    '<div class="item-title">' + json[i].subject + '</div>' +
                                    '<div class="item-subtitle">'+ json[i].from +'</div>' +
                                '</div>' +
                            '<div class="item-subtitle">' + displaydate + '</div>' +
                            '</div>' +
                        '</a>' +
                        '<div class="accordion-item-content">' +
                            '<div class="content-block"><div class="row">' +
                                '<div class="col-30">Date:</div><div class="col-70">' + json[i].created_at + '</div>' +
                                '<div class="col-30">To:</div><div class="col-70">' + json[i].to + '</div>' +
                                '<div class="col-30">From:</div><div class="col-70">' + json[i].from + '</div>' +
                                '<div class="col-30">Priority:</div><div class="col-70">' + json[i].priority + '</div>' +
                                '<p>Message: <br>' + json[i].body + '</p>' +
                            '</div></div>' +
                        '</div>' +
                        '</li>';
                    $$(target).append(ili);

                }

                $$('.message-count').html(json.length);
            }
        );

    },

    getMeetings: function(myApp, url, target, count) {
        $$.get(
            url + '/api/tiger/meeting',
            {},
            function (data) {

                var json = JSON.parse(data);

                if ( count == -1) {
                    itmctr = json.length;
                } else {
                    itmctr = count;
                }
                $$(target).html('');
                for (i = 0; i < itmctr; i++) {
                    var sent = new Date(json[i].start);
                    var month = sent.getMonth() + 1;
                    var day = sent.getDate();
                    var displaydate = month + '/' + day;
                    var acceptButton = '';
                    var username = localStorage.getItem('username');

                    if (username == 'Carlos' && json[i].status != 'Accepted') {
                        acceptButton = '<button type="button" class="button acceptButton button-round" data-id="' + json[i].id + '" />Accept</button><br/>';
                    }

                    var ili = '<li class="accordion-item">' +
                        '<a href="#" class="item-link item-content">' +
                        '<div class="item-inner">' +
                        '<div class="item-title-row">' +
                        '<div class="item-title">' + json[i].agenda + '</div>' +
                        //'<div class="item-subtitle">' + json[i].from + '</div>' +
                        '</div>' +
                        '<div class="item-subtitle">' + json[i].status + '</div>' +
                        '</div>' +
                        '</a>' +
                        '<div class="accordion-item-content">' +
                        '<div class="content-block"><div class="row">' +
                        '<div class="col-30">Date:</div><div class="col-70">' + json[i].created_at + '</div>' +
                        '<div class="col-30">To:</div><div class="col-70">' + json[i].to + '</div>' +
                        '<div class="col-30">From:</div><div class="col-70">' + json[i].from + '</div>' +
                        '<div class="col-30">Start (PST):</div><div class="col-70">' + json[i].start + '</div>' +
                        '<div class="col-30">End (PST):</div><div class="col-70">' + json[i].end + '</div>' +
                        '<div class="col-30">Venue:</div><div class="col-70">' + json[i].venue + '</div>' +
                        '<p>Remarks: <br>' + json[i].remarks + '</p>' +
                        '</div><p>' + acceptButton + '</p></div>' +
                        '</div>' +
                        '</li>';
                    $$(target).append(ili);

                }

                $$('.meeting-count').html(json.length);

                $$('.acceptButton').on('tap, click', function(e){
                    e.preventDefault();
                    var mid = $(this).data('id');
                    console.log(mid);

                    $.post(
                        app.url + '/api/tiger/meeting/confirm',
                        {
                            "id" : mid,
                            "status" : "Accepted"
                        },
                        function(res) {
                            console.log(res);
                            myApp.alert('Meeting schedule accepted');
                            tiger.getMeetings(myApp, app.url, '.meeting-container', 3);
                        }

                    );
                });

            }
        );
    },
    initTimePicker: function(target) {
        var today = new Date();
        var hour = today.getHours();
        for(h = 1; h <= 24; h++) {
            for (m = 0; m <= 45; m=m+15) {
                var min = (m==0) ? '00' : m;
                var tim = h + ':' + min;
                var strTime = '<option ';
                strTime = strTime + 'value="' + tim + '"';

                if (h == hour && min =='00') {
                    strTime = strTime +' selected ';
                }

                strTime = strTime + '>' + tim + '</option>';

                $(target).append(strTime);
            }
        }
    }
};
