
var app = {
    //dummy data
    url: 'http://malasakitdev.appteknik.com',
    //url: 'http://mala.sytes.net',
    users: ["Exec", "Carlos"],

    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },

    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },

    onDeviceReady: function() {
        app.receivedEvent('deviceready');

        $$ = Dom7;

        var myApp = new Framework7({
            pushState: true,
            modalTitle: 'Tiger Resort RT',
            // Hide and show indicator during ajax requests
            onAjaxStart: function (xhr) {
                myApp.showIndicator();
            },
            onAjaxComplete: function (xhr) {
                myApp.hideIndicator();
            }
        });

        var mainView = myApp.addView('.view-main', {
            dynamicNavbar: true
        });


        var pageContainer = $$('.login-screen');

        pageContainer.find('.list-button').on('tap, click', function () {
            var username = pageContainer.find('input[name="username"]').val();
            var password = pageContainer.find('input[name="password"]').val();
            // Handle username and password

            if ($.inArray(username, app.users) >= 0 && password == '12345') {
                localStorage.setItem('loggedIn', "true");
                localStorage.setItem('username', username);
                myApp.closeModal($$('.login-screen'));
                tiger.showAllProjects(mainView);
                tiger.getMessages(app.url, '.message-container', 3);
                tiger.getMeetings(myApp, app.url, '.meeting-container', 3);

                if (username=='Exec') {
                    $$('.meeting-footer').append('<span><a href="newmeeting.html">Schedule New Meeting</a></span>');
                }


            } else {
                myApp.alert('Invalid credentials', 'Login Error', function(){
                    pageContainer.find('input[name="username"]').val('');
                    pageContainer.find('input[name="password"]').val('');
                });
            }

        });

        myApp.onPageInit('charts', function (page) {
            tiger.showRedFlagByStatus(mainView);
            tiger.showRedFlagByVariance(mainView);
            tiger.showAllByStatus(mainView);
            tiger.showAllByVariance(mainView);
        });

        myApp.onPageAfterAnimation('redflags', function(page){

            if (page.url.indexOf('#') > 0) {
                var url = page.url;
                var loc = url.substr( url.indexOf('#') );
                $$('.page-content').scrollTop($$(loc).offset().top, 500);
            }

        });

        myApp.onPageInit('newmessage', function(page){
            $$('input[name="from"]').val(localStorage.getItem('username'));
            $$('input[type=submit]').on('click', function (e) {
                e.preventDefault();
                var data = myApp.formToJSON('#newmessage');
                console.log(data);
                $.post(
                    app.url + '/api/tiger/message',
                    data,
                    function(res) {
                        console.log(res);
                        myApp.alert('Message submitted');
                        mainView.router.back();
                        tiger.getMessages(app.url, '.message-container', 3);
                    }
                );

            });
        });

        myApp.onPageInit('newmeeting', function(page){
            $$('input[name="from"]').val(localStorage.getItem('username'));
            var today = new Date();
            var mon = today.getMonth() + 1;
            mon = (mon < 10) ? '0' + mon : mon;
            var strDate =  today.getFullYear()+ '-' + mon + '-' + today.getDate() ;
            $$('#startdate').val(strDate);
            $$('#enddate').val(strDate);
            tiger.initTimePicker('#starttime');
            tiger.initTimePicker('#endtime');
            $$('input[type=submit]').on('click', function (e) {
                e.preventDefault();
                var data = myApp.formToJSON('#newmeeting');
                data.start = data.startdate + ' ' + data.starttime;
                data.end = data.enddate + ' ' + data.endtime;
                $.post(
                    app.url + '/api/tiger/meeting',
                    data,
                    function(res) {
                        console.log(res);
                        myApp.alert('Meeting submitted');
                        mainView.router.back();
                        tiger.getMeetings(myApp, app.url, '.meeting-container', 3);
                    }
                );

            });
        });

        myApp.onPageInit('index', function(page){
            tiger.showAllProjects(mainView);
            tiger.getMessages(app.url, '.message-container', 3);
            tiger.getMeetings(myApp, app.url, '.meeting-container', 3);
            var username = localStorage.getItem('username');
            if (username=='Exec') {
                $$('.meeting-footer').append('<span><a href="newmeeting.html">Schedule New Meeting</a></span>');
            }
        });

        myApp.onPageInit('messages', function(page){
            tiger.getMessages(app.url, '.message-container-all', -1);
        });

        myApp.onPageInit('meetings', function(page){
            tiger.getMeetings(myApp, app.url, '.meeting-container-all', -1);
            var username = localStorage.getItem('username');
            if (username=='Exec') {
                $$('.schedule-button-container').append('<a href="newmeeting.html" class="button button-round active">Schedule New Meeting</a>');
            }
        });

        //var fetchInt = setInterval(function(){
        //    console.log('fetching from server');
        //    tiger.getMessages(app.url, '.message-container', 3);
        //    tiger.getMeetings(myApp, app.url, '.meeting-container', 3);
        //}, 180000);

    },
    receivedEvent: function(id) {
        var parentElement = document.getElementById(id);
    }


};

app.initialize();
